package fr.univtours.polytech.commerce.dao;

public interface UserDAO {
    boolean validateUser(String login, String password);
}
