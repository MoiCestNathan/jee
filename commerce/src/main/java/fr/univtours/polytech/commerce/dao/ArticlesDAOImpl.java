
// package fr.univtours.polytech.commerce.dao;

// import java.util.List;

// import fr.univtours.polytech.commerce.model.ArticleBean;

// public class ArticlesDAOImpl implements ArticlesDAO{
//     public void addArticle(ArticleBean article) {
//         // TODO Auto-generated method stub
//     }

//     public void updateArticle(ArticleBean article) {
//         // TODO Auto-generated method stub
//     }

//     public void deleteArticle(ArticleBean article) {
//         // TODO Auto-generated method stub
//     }

//     public ArticleBean getArticleById(int id) {
//         // TODO Auto-generated method stub
//         return null;
//     }

//     public List<ArticleBean> getAllArticles() {
//         // TODO Auto-generated method stub
//         return null;
//     }

// }


package fr.univtours.polytech.commerce.dao;

import java.util.List;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;

import fr.univtours.polytech.commerce.model.ArticleBean;

@Transactional
public class ArticlesDAOImpl implements ArticlesDAO {

    @PersistenceContext
    private EntityManager entityManager;

    public List<ArticleBean> getAllArticles() {
        return entityManager.createQuery("SELECT a FROM Article a", ArticleBean.class).getResultList();
    }

    public ArticleBean getArticleById(int id) {
        return entityManager.find(ArticleBean.class, id);
    }

    public void updateStock(int articleId, int newStock) {
        ArticleBean article = entityManager.find(ArticleBean.class, articleId);
        article.setStock(newStock);
        entityManager.merge(article);
    }
}
