package fr.univtours.polytech.commerce.business;

import fr.univtours.polytech.commerce.dao.UserDAO;
import fr.univtours.polytech.commerce.dao.UserDAOImpl;

public class UserService {

    private UserDAO userDAO;

    public UserService() {
        this.userDAO = new UserDAOImpl();
    }

    public boolean validateUser(String login, String password) {
        return userDAO.validateUser(login, password);
    }
}
