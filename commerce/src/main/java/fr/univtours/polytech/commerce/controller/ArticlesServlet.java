
package fr.univtours.polytech.commerce.controller;

import java.io.IOException;
import java.util.List;

import fr.univtours.polytech.commerce.business.ArticlesService;
import fr.univtours.polytech.commerce.model.ArticleBean;
import fr.univtours.polytech.commerce.dao.ArticlesDAO;
import fr.univtours.polytech.commerce.dao.ArticlesDAOImpl;
import jakarta.inject.Inject;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

// @WebServlet(name = "articlesServlet", urlPatterns = { "/articles" })
// public class ArticlesServlet extends HttpServlet {

//     @Inject
//     private ArticlesService articlesService;

//     @Override
//     protected void doGet(HttpServletRequest request, HttpServletResponse response)
//             throws ServletException, IOException {
                
        



        
        
//         //List<ArticleBean> articles = null;
//         // List<ResultBean> results = business.getResultsList();

//         // request.setAttribute("RESULTS_LIST", results);
//         // request.setAttribute("RESULTS_MEAN", business.computeMean(results));

//         request.getRequestDispatcher("articles.jsp").forward(request, response);
//     }
// }

@WebServlet(name = "articlesServlet", urlPatterns = { "/articles" })
public class ArticlesServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    // Inject ArticlesDAOImpl using @Inject or @Autowired annotation
    @Inject
    private ArticlesDAOImpl articleDAO;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Call DAO to get list of articles
        List<ArticleBean> articles = articleDAO.getAllArticles();
        
        // Pass articles to JSP
        request.setAttribute("articles", articles);
        request.getRequestDispatcher("articles.jsp").forward(request, response);
    }
}
