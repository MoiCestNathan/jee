// package fr.univtours.polytech.commerce.dao;

// import java.util.List;

// import fr.univtours.polytech.commerce.model.ArticleBean;

// public interface ArticlesDAO {
//     public void addArticle(ArticleBean article);
//     public void updateArticle(ArticleBean article);
//     public void deleteArticle(ArticleBean article);
//     public ArticleBean getArticleById(int id);
//     public List<ArticleBean> getAllArticles();

// }

package fr.univtours.polytech.commerce.dao;

import java.util.List;
import fr.univtours.polytech.commerce.model.ArticleBean;

public interface ArticlesDAO {
    List<ArticleBean> getAllArticles();
    ArticleBean getArticleById(int id);
    void updateStock(int articleId, int newStock);
}
