package fr.univtours.polytech.commerce.controller;

import fr.univtours.polytech.commerce.business.UserService;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {

    private UserService userService;

    @Override
    public void init() throws ServletException {
        super.init();
        userService = new UserService();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        String password = request.getParameter("password");

        boolean isValidUser = userService.validateUser(login, password);

        if (isValidUser) {
            request.getSession().setAttribute("user", login);
            response.sendRedirect("articles.jsp");
        } else {
            request.setAttribute("errorMessage", "Le nom d'utilisateur ou le mot de passe saisit n'est pas valide.");
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }
    }
}
