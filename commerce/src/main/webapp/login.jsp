<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>Connexion</title>
</head>
<body>
<h2>Connexion à votre compte</h2>
<form action="login" method="post">
    <label for="login">Login:</label>
    <input type="text" id="login" name="login"><br>
    <label for="password">Mot de passe:</label>
    <input type="password" id="password" name="password"><br>
    <input type="submit" value="Connexion">
</form>
<% if (request.getAttribute("errorMessage") != null) { %>
    <p style="color: red;"><%= request.getAttribute("errorMessage") %></p>
<% } %>
</body>
</html>
