<!-- <%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Articles</title>
</head>
<body>
    <div id="username-display">
        <p>Connecté en tant que : ${username}</p>
        <a href="/logout">Déconnexion</a>
    </div>

    <h1>Liste des articles</h1>
    <div id="articles-frame">
    <table border="1">
        <tr>
            <th>Nom</th>
            <th>Prix</th>
            <th>Nombre restant</th>
            <th>Enlever 1 au panier</th>
            <th>Panier actuel</th>
            <th>Ajouter 1 au panier</th>
        </tr>
        <c:forEach items="${articles}" var="article">
            <tr>
                <td>${article.nom}</td>
                <td>${article.prix}</td>
                <td>${article.nbRestant}</td>
                <td>
                    <a href="/enlever-1-au-panier?id=${article.id}">-</a>
                </td>
                <td>${article.nbDansPanier}</td>
                <td>
                    <a href="/ajouter-1-au-panier?id=${article.id}">+</a>
            </tr>
        </c:forEach>
    </table>
</div>

<button id="afficher-panier">Afficher Panier</button>
</body>
</html> -->
<%@ page contentType="text/html; charset=UTF-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Liste des Articles</title>
</head>
<body>
    <h1>Liste des Articles</h1>
    <table border="1">
        <tr>
            <th>Nom</th>
            <th>Prix</th>
            <th>Stock</th>
        </tr>
        <%-- Loop through articles and display them --%>
        <c:forEach items="${articles}" var="article">
            <tr>
                <td>${article.name}</td>
                <td>${article.price}</td>
                <td>${article.stock}</td>
            </tr>
        </c:forEach>
    </table>
    <br>
    <a href="ViewCartServlet">Voir le Panier</a>
    <br>
    <a href="LogoutServlet">Déconnexion</a>
    <br>
    <p>Connecté en tant que : ${username}</p>
</body>
</html>
